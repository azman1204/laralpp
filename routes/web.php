<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// http://laralpp/
Route::get('/', function () {
    return view('welcome');
});

// http://laralpp/salam
Route::get('/salam', function () {
    return view('assalam'); // /resources/views/assalam.blade.php
});

Route::get('/login', function () {
    return view('login');
});

Route::post('/login', function () {
    $credentials = request()->only('email', 'password');
    if (\Auth::attempt($credentials)) {
        //echo 'ok';
        return view('home');
    } else {
        echo 'failed';
    }
});
